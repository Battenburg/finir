import Vue from 'vue';
import Router from 'vue-router';
import Timeline from '@/components/Timeline';
import Settings from '@/components/Settings';
import Debug from '@/components/Debug';
import Rewards from '@/components/Rewards';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Timeline',
      component: Timeline,
    },
    {
      path: '/settings',
      name: 'Settings',
      component: Settings,
    },
    {
      path: '/debug',
      name: 'Debug',
      component: Debug,
    },
    {
      path: '/rewards',
      name: 'Rewards',
      component: Rewards,
    },
  ],
});
